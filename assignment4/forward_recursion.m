function [w, dw, ddp, ddpc] = forward_recursion(w0, dw0, data)
%FORWARD_RECURSION Computes the forward recursion for the Newton-Euler
%formulation

syms q1 q2 q3 dq1 dq2 dq3 ddq1 ddq2 ddq3

z0 = [0;0;1];
ddp0 = -data.g;

Ab0 = data.Ab0;
A01 = data.A01;
A12 = data.A12;
A23 = data.A23;
A3e = data.A3e;

Rb0 = Ab0(1:3, 1:3);
R01 = A01(1:3, 1:3);
R12 = A12(1:3, 1:3);
R23 = A23(1:3, 1:3);


% FORWARD RECURSION ALGORITHM
% Revolute joint 1

R01 = data.A01(1:3, 1:3);
r01 = data.A01(1:3, 4);
r11 = R01.' * r01;

r1c1 = -data.com1.';

w11 = R01.' * w0 + R01.' * dq1 * z0;
dw11 = R01.' * dw0 + R01.' * (ddq1 * z0 + cross(dq1 * w0, z0));
ddp11 = R01.' * ddp0 + cross(dw11, r11) + cross(w11, cross(w11, r11));
ddp1c1 = ddp11 + cross(dw11, r1c1) + cross(w11, cross(w11, r1c1));

% Prismatic joint 2

R12 = data.A12(1:3, 1:3);
r12 = data.A12(1:3, 4);
r22 = R12.' * r12;

r2c2 = -data.com2.';

w22 = R12.' * w11;
dw22 = R12.' * dw11;
ddp22 = R12.' * ddp11 + cross(dw22, r22) + cross(w22, cross(w22, r22)) + R12.' * ddq2 * z0 + cross(2 * dq2 * w22, R12.' * z0);
ddp2c2 = ddp22 + cross(dw22, r2c2) + cross(w22, cross(w22, r2c2));

% Revolute joint 3
R23 = data.A23(1:3, 1:3);
r23 = data.A23(1:3, 4);
r33 = R23.' * r23;

r3c3 = -data.com3.';

w33 = R23.' * w22 + R23.' * dq3 * z0;
dw33 = R23.' * dw22 + R23.' * (ddq3 * z0 + cross(dq3 * w22, z0));
ddp33 = R23.' * ddp22 + cross(dw33, r33) + cross(w33, cross(w33, r33));
ddp3c3 = ddp33 + cross(dw33, r3c3) + cross(w33, cross(w33, r3c3));

w = [w11 w22 w33];
dw = [dw11 dw22 dw33];
ddp = [ddp11 ddp22 ddp33];
ddpc = [ddp1c1 ddp2c2 ddp3c3];

end


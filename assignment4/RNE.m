function [tau] = RNE(w0, dw0, he, data)
%RNE Computes the Newton Euler Formulation
[w, dw, ddp, ddpc] = forward_recursion(w0, dw0, data);
[tau] = backward_recursion(w, dw, ddp, ddpc, he, data);

end


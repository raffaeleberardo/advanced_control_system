function [tau] = backward_recursion(w, dw, ddp, ddpc, he, data)
%BACKWARD_RECURSION to propagate forces from frame n to 0.
fe = he(1:3);
ue = he(4:6);
z0 = [0;0;1];

% Joint 3
m3 = data.m3;
I3 = data.I3;

R3e = data.A3e(1:3, 1:3);
R23 = data.A23(1:3, 1:3);
r23 = data.A23(1:3, 4);
r33 = R23.' * r23;
r3c3 = -data.com3.';


f33 = R3e * fe + m3*ddpc(:, 3);
u33 = cross(-f33, r33 + r3c3) + R3e * ue + cross(R3e * fe, r3c3) + I3*dw(:,3) + cross(w(:, 3), I3*w(:,3));
tau3 = u33.'*R23.'*z0;

% Joint 2
m2 = data.m2;
I2 = data.I2;
R12 = data.A12(1:3, 1:3);
r12 = data.A12(1:3, 4);
r22 = R12.' * r12;
r2c2 = -data.com2.';

f22 = R23*f33 + m2*ddpc(:, 2);
u22 = cross(-f22, r22 + r2c2) + R23*u33 + cross(R23*f33, r2c2) + I2*dw(:, 2) + cross(w(:,2), I2 * w(:,2));
tau2 = f22.'*R12.'*z0;

% Joint 1
m1 = data.m1;
I1 = data.I1;
R01 = data.A01(1:3, 1:3);
r01 = data.A01(1:3, 4);
r11 = R01.' * r01;
r1c1 = -data.com1.';

f11 = R12 * f22 + m1 * ddpc(:, 1);
u11 = cross(-f11, r11 + r1c1) + R12*u22 + cross(R12*f22, r1c1) + I1*dw(:,1) + cross(w(:,1), I1*w(:,1));
tau1 = u11.'*R01.'*z0;

tau = [tau1; tau2; tau3];

end


function [Cn] = ComputeCoriolisMatrix(q, dq)
% Compute the Coriolis matrix for the RPR manipulator numerically
q1 = q(1); 
q2 = q(2); 
q3 = q(3);
dq1 = dq(1); 
dq2 = dq(2); 
dq3 = dq(3);
Cn = [-(45216*dq3*sin(q3))/390625, 0, -(45216*sin(q3)*(dq1 + dq3))/390625;
                         0, 0,                                   0;
 (45216*dq1*sin(q3))/390625, 0,                                   0];
end

function [Gn] = ComputeGravityTerm(q)
% Compute the Gravity term for the RPR manipulator numerically
Gn = [0; 44.8466; 0];
end


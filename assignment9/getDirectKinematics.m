function x = getDirectKinematics(q)

    l0 = 0.15;
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24; 
    
    q1 = q(1);
    q2 = q(2);
    q3 = q(3);
    
    dk = [[6.1e-17*cos(q1)*cos(q3) - 6.1e-17*sin(q1)*sin(q3), - 1.0*cos(q1)*sin(q3) - 1.0*cos(q3)*sin(q1), cos(q1)*cos(q3) - 1.0*sin(q1)*sin(q3), l1*cos(q1) + l3*cos(q1)*cos(q3) - 1.0*l3*sin(q1)*sin(q3)];
        [6.1e-17*cos(q1)*sin(q3) + 6.1e-17*cos(q3)*sin(q1),       cos(q1)*cos(q3) - 1.0*sin(q1)*sin(q3),     cos(q1)*sin(q3) + cos(q3)*sin(q1),     l1*sin(q1) + l3*cos(q1)*sin(q3) + l3*cos(q3)*sin(q1)];
        [                                             -1.0,                                           0,                               6.1e-17,                                             l0 + l2 + q2];
        [                                                0,                                           0,                                     0,                                                      1.0]];
    
    phi = [
    atan2(dk(2,3), dk(1,3));
    atan2(sqrt(dk(1,3)^2+dk(2,3)^2),dk(3,3));
    atan2(dk(3,2), -dk(3,1));
    ];

    x = [dk(1:3, 4); phi];

end


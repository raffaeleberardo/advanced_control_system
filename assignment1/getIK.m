% Function to compute the inverse kinematics given the minimum parameters
% pose: position + angle
function configuration = getIK()
    syms l0 l1 l2 l3 q1 q2 q3 p_x p_y p_z phi
    
    c3 = (p_x^2+p_y^2-l3^2-l1^2)/(2*l1*l3);
    s3 = real(sqrt(1-(c3^2))); % elbow up
    
    q3_up = atan2(s3, c3); % elbow up
    q3_down = atan2(-s3, c3); % elbow down
    q1 = phi - q3_up;
    q1_down = phi - q3_down;
    
    q2 = p_z - (l0 + l2);
    configuration = [q1 q2 q3_up; q1_down q2 q3_down];
end
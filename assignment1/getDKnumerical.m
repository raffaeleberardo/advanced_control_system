function [dk_numerical, Ab0, A01, A12, A23, A3e] = getDKnumerical(q)

    l0 = 0.15;
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24; 
    
    q1 = q(1);
    q2 = q(2);
    q3 = q(3);

    c1 = cos(q1);
    s1 = sin(q1);

    c3 = cos(q3);
    s3 = sin(q3);


    Ab0 = [
        1 0 0 0;
        0 1 0 0;
        0 0 1 l0;
        0 0 0 1
    ];

    A01 = [
        c1 -s1 0 l1*c1;
        s1 c1 0 l1*s1;
        0 0 1 0;
        0 0 0 1
    ];

    A12 = [
        1 0 0 0;
        0 1 0 0;
        0 0 1 l2+q2;
        0 0 0 1
    ];

    A23 = [
        c3 -s3 0 l3*c3;
        s3 c3 0 l3*s3;
        0 0 1 0;
        0 0 0 1
    ];

    % Final transformation to rotate our final frame in the configuration
    % normal slide approach
    A3e = [
        cos(pi/2) 0 sin(pi/2)   0;
        0         1     0       0;
       -sin(pi/2) 0  cos(pi/2)  0;
        0         0     0       1
    ];
    
    dk_numerical = [[6.1e-17*cos(q1)*cos(q3) - 6.1e-17*sin(q1)*sin(q3), - 1.0*cos(q1)*sin(q3) - 1.0*cos(q3)*sin(q1), cos(q1)*cos(q3) - 1.0*sin(q1)*sin(q3), l1*cos(q1) + l3*cos(q1)*cos(q3) - 1.0*l3*sin(q1)*sin(q3)];
        [6.1e-17*cos(q1)*sin(q3) + 6.1e-17*cos(q3)*sin(q1),       cos(q1)*cos(q3) - 1.0*sin(q1)*sin(q3),     cos(q1)*sin(q3) + cos(q3)*sin(q1),     l1*sin(q1) + l3*cos(q1)*sin(q3) + l3*cos(q3)*sin(q1)];
        [                                             -1.0,                                           0,                               6.1e-17,                                             l0 + l2 + q2];
        [                                                0,                                           0,                                     0,                                                      1.0]];
    
end


% Compute the Analytical Jacobian
function JA = getJA()
    syms l0 l1 l2 l3 q1 q2 q3
    
    c1 = cos(q1);
    s1 = sin(q1);

    c3 = cos(q3);
    s3 = sin(q3);
    
    c13 = cos(q1 + q3);
    s13 = sin(q1 + q3);
    
    JA = [[-l3*s13 - l1*s1; l3*c13 + l1*c1; 0; 1], [0;0;1;0], [-l3*s13;l3*c13;0;1]];

end
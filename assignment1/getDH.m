function [DH] = getDH()
% Return the DH table for the RPR manipulator
    syms l0 l1 l2 l3 q1 q2 q3
    theta = {0; q1; 0; q3};
    d = {l0; 0; l2+q2; 0};
    alpha = {0; 0; 0; 0};
    a = {0; l1; 0; l3};
    
    DH.theta = [0; q1; 0; q3];
    DH.d = [l0; 0; l2+q2; 0];
    DH.alpha = [0;0;0;0];
    DH.a = [0;l1;0;l3];
end


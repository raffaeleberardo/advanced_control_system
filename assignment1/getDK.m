% Function to compute the direct kinematics of my RPR manipulator
function [dk, Ab0, A01, A12, A23, A3e] = getDK()    
    syms l0 l1 l2 l3 q1 q2 q3
    
    c1 = cos(q1);
    s1 = sin(q1);

    c3 = cos(q3);
    s3 = sin(q3);


    Ab0 = [
        1 0 0 0;
        0 1 0 0;
        0 0 1 l0;
        0 0 0 1
    ];

    A01 = [
        c1 -s1 0 l1*c1;
        s1 c1 0 l1*s1;
        0 0 1 0;
        0 0 0 1
    ];

    A12 = [
        1 0 0 0;
        0 1 0 0;
        0 0 1 l2+q2;
        0 0 0 1
    ];

    A23 = [
        c3 -s3 0 l3*c3;
        s3 c3 0 l3*s3;
        0 0 1 0;
        0 0 0 1
    ];

    % Final transformation to rotate our final frame in the configuration
    % normal slide approach
    A3e = [
        cos(pi/2) 0 sin(pi/2)   0;
        0         1     0       0;
       -sin(pi/2) 0  cos(pi/2)  0;
        0         0     0       1
    ];

    dk = vpa(Ab0*A01*A12*A23*A3e, 2);
end



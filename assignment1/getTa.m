function [Ta] = getTa(q)
% Ta is the transformation matrix which relates the geometric with the
% analytical jacobian. We consider the ZYZ euler combination.
    dk = getDKnumerical(q);
    Rbe = dk(1:3, 1:3);
    eulZYZ = rotm2eul(Rbe,'ZYZ');
    phi = eulZYZ(1);
    theta = eulZYZ(2);
    gamma = eulZYZ(3);
    T = [0 -sin(phi) cos(phi)*sin(theta); 
          0 cos(phi) sin(phi)*sin(theta); 
          1 0 cos(theta)];
     Ta = [eye(3) zeros(3); zeros(3) T]; 
end


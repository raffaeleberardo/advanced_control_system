function [J] = getJGnumerical(q)
    l0 = 0.15; 
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24;
    
    q1 = q(1);
    q2 = q(2);
    q3 = q(3);
    
    c1 = cos(q1);
    s1 = sin(q1);

    c3 = cos(q3);
    s3 = sin(q3);
    
    c13 = cos(q1 + q3);
    s13 = sin(q1 + q3);
    S = [0 -1 0; 1 0 0; 0 0 0];
    J = [[S*[l3*c13 + l1*c1; l3*s13+l1*s1; l2+q2;]; 0;0;1;], [0;0;1; 0;0;0;], [[S*[l3*c13;l3*s13;0;]; 0;0;1;]]];
end
function [configuration] = getIKnumerical(pose)
    l0 = 0.15;
    l1 = 0.4;
    l2 = 0.3;
    l3 = 0.24;

    px = pose(1);
    py = pose(2);
    pz = pose(3);
    phi = pose(4); 
    
    c3 = (px^2+py^2-l3^2-l1^2)/(2*l1*l3);
    s3 = real(sqrt(1-(c3^2))); % elbow up
    
    q3_up = atan2(s3, c3); % elbow up
    q3_down = atan2(-s3, c3); % elbow down
    q1 = phi - q3_up;
    q1_down = phi - q3_down;
    
    q2 = pz - (l0 + l2);
    configuration = round([q1 q2 q3_up; q1_down q2 q3_down], 4);
end


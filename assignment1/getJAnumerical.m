function [JA] = getJAnumerical(q)
    l0 = 0.15;
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24;
    
    q1 = q(1);
    q2 = q(2);
    q3 = q(3);
    
    JA = [[- l3*sin(q1 + q3) - l1*sin(q1), 0, -l3*sin(q1 + q3)];
          [  l3*cos(q1 + q3) + l1*cos(q1), 0,  l3*cos(q1 + q3)];
          [                             0, 1,                0];
          [                             0, 0,                0];
          [                             0, 0,                0];
          [                             1, 0,                1]];
end


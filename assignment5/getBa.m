function [Ba] = getBa(data)
    B = data.B;
    Ta = data.Ta;
    J = data.J;
    Ja = inv(Ta) * J;
    Ba = pinv(Ja * inv(B) * Ja.');
end


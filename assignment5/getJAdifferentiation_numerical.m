function [dJA_num] = getJAdifferentiation_numerical(q, dq)
    l0 = 0.15;
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24;
    
    q1 = q(1);
    q2 = q(2);
    q3 = q(3);

    dq1 = dq(1);
    dq2 = dq(2);
    dq3 = dq(3);

    c1 = cos(q1);
    s1 = sin(q1);

    c3 = cos(q3);
    s3 = sin(q3);
    
    c13 = cos(q1 + q3);
    s13 = sin(q1 + q3);

    dJA_num = [[-l3*c13*(dq1 + dq3) - l1*c1*dq1; -l3*s13*(dq1 + dq3) - l1*s1*dq1; 0; 0], [0;0;0;0], [-l3*c13*(dq1 + dq3);-l3*s13*(dq1 + dq3);0;0]];

end


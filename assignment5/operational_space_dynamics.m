function [data] = operational_space_dynamics(data)
    Ta = getTa(data.q);
    data.Ba = getBa(data);
    data.Ca = getCa(data);
    data.Ga = getGa(data);
    h = data.h;
    data.u = Ta.' * h; % h are torques at EE tau = J.'*h
    he = data.he;
    data.ue = Ta.' * he;
end


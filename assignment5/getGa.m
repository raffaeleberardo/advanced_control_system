function [Ga] = getGa(data)
    Ba = data.Ba;
    Ta = getTa(data.q);
    J = data.J;
    Ja = inv(Ta) * J;
    B = data.B;
    G  = data.G;
    Ga = Ba * Ja * inv(B) * G;
end


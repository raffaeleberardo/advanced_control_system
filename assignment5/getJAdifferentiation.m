function [dJA] = getJAdifferentiation()
    syms l0 l1 l2 l3 q1 q2 q3 dq1 dq2 dq3
    
    dJA =   [[- l3*cos(q1 + q3)*(dq1+dq3) - l1*cos(q1)*dq1, 0, -l3*cos(q1 + q3)*(dq1+dq3)];
            [- l3*sin(q1 + q3)*(dq1+dq3) - l1*sin(q1)*dq1, 0, -l3*sin(q1 + q3)*(dq1+dq3)];
            [                             0, 0,                0];
            [                             0, 0,                0];
            [                             0, 0,                0];
            [                             0, 0,                0]];
end


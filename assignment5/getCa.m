function [Ca] = getCa(data)
    dq = data.dq;
    Ta = getTa(data.q);
    J = data.J;
    Ja = inv(Ta) * J;
    Ba = data.Ba;
    B = data.B;
    C = data.C;
    C1 = (Ba * Ja * inv(B) * C * dq);
%     Ja_diff = getJAdifferentiation_numerical(q, dq);
    Jd = data.Jadot;
    C2 = (Ba * Jd * dq);
    Ca = C1 - C2;
end


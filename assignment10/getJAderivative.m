function dJA = getJAderivative(q,dq)
l0 = 0.15;
l1 = 0.4; 
l2 = 0.3; 
l3 = 0.24;

q1 = q(1);
q2 = q(2);
q3 = q(3);

dq1 = dq(1);
dq2 = dq(2);
dq3 = dq(3);

% JA = [[- l3*sin(q1 + q3) - l1*sin(q1), 0, -l3*sin(q1 + q3)];
%       [  l3*cos(q1 + q3) + l1*cos(q1), 0,  l3*cos(q1 + q3)];
%       [                             0, 1,                0];
%       [                             0, 0,                0];
%       [                             0, 0,                0];
%       [                             1, 0,                1]];

dJA = [[- l3*cos(q1 + q3)*(dq1 + dq3) - l1*cos(q1)*dq1, 0, -l3*cos(q1 + q3)*(dq1 + dq3)];
      [  -l3*sin(q1 + q3)*(dq1 + dq3) - l1*sin(q1)*dq1, 0,  -l3*sin(q1 + q3)*(dq1 + dq3)];
      [                             0, 0,                0];
      [                             0, 0,                0];
      [                             0, 0,                0];
      [                             0, 0,                0]];
end


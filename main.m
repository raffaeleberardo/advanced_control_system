% help show
% help rigidBodyJoint
% help rigidBodyTree
% help rigidBody

% https://it.mathworks.com/help/robotics/ug/rigid-body-tree-robot-model.html#responsive_offcanvas
% https://it.mathworks.com/help/robotics/ref/rigidbodytree.html
% https://it.mathworks.com/help/robotics/ref/rigidbody.html
% https://it.mathworks.com/help/robotics/ref/rigidbodytree.show.html

close all; 
clear all; 
clc;

addpath('trajectories', ...
        'assignment1\', ...
        'assignment2\', ...
        'assignment3\',...
        'assignment4\', ...
        'assignment5\', ...
        'assignment6\', ...
        'assignment7\', ...
        'assignment8', ...
        'assignment9', ...
        'assignment10', ...
        'assignment11', ...
        'assignment12', ...
        'assignment13', ...
        'assignment14');

%% Robot import and configuration settings
robot = importrobot('RPR_2.urdf');
showdetails(robot)
figure;
% config = homeConfiguration(robot);
config = randomConfiguration(robot);
show(robot,config);
% xlim([-0.5 0.8])
% ylim([-0.5 0.5])
zlim([0 0.8])


q = [config(1).JointPosition;
    config(2).JointPosition;
    config(3).JointPosition]

dq = rand([3,1]) * 10;
ddq = rand([3,1]) * 10;

% q = [0;0;0];
% dq = [0;0;0];
% ddq = [0;0;0];

l0 = 0.15;
l1 = 0.4;
l2 = 0.3;
l3 = 0.24;

r1 = 0.02;
r3 = 0.02;

% Mass of each link

density = 8000; % steel density

data.m1 = 3.14 * r1^2 * l1 * density; % kg/m3; 
data.m2 = 0.03 * 0.03 * l2 * density ;
data.m3 = 3.14 * r3^2 * l3 * density;


% % FOR WRONG MEASUREMENTS
% 
% % Mass of each link
% 
% density = 8000000; 
% 
% data.m1 = 3.14 * r1^2 * l1 * density; %kg/m3; 
% data.m2 = 0.03 * 0.03 * l2 * density ;
% data.m3 = 3.14 * r3^2 * l3 * density;
% 
% % END OF WRONG MEASUREMENTS

% Initialize parameters for robot
data.links = [l0;l1;l2;l3];
data.q = q; % robot positions
data.dq = dq; % robot velocities
data.ddq = ddq; % robot accelerations

%% Assignment 1
data.DH = getDH(); % Denavit Haetenberg
[data.dk, data.Ab0, data.A01, data.A12, data.A23, data.A3e] = getDK(); % Direct Kinematics
data.ik = getIK(); % Inverse Kinematics
data.J = getJG(); % Geometric Jacobian
data.Ja = getJA(); % Analytical Jacobian


disp("-------------------------------------------------------------------")
% Direct kinematics computed using the toolbox
disp("ROBOTICS TOOLBOX MATLAB FUNCTION:");
dk_tool = round(getTransform(robot,config,char(robot.BodyNames(4))),2);
% Direct kinematics computed using a personal function
disp("MANUAL COMPUTATIONS DIRECT KINEMATICS FUNCTION:");
dk = data.dk
[dk_num, Ab0, A01, A12, A23, A3e] = getDKnumerical(q);
dk_num = round(dk_num, 2)
if (dk_num == dk_tool)
    disp("Robotics toolbox DK computations equal to mine.")
end

disp("INVERSE KINEMATICS SECTION");

angle = q(1) + q(3);
disp("POSE");

pose = [dk_num(1:end-1, end).' angle]
%Toolbox
ik_tool = inverseKinematics('RigidBodyTree',robot);
weights = [0.25 0.25 0.25 1 1 1];
disp("INVERSE KINEMATICS COMPUTED WITH TOOLBOX");
[configSoln,solnInfo] = ik_tool('ee',dk_tool, weights, config);
toolbox_config = [configSoln(1).JointPosition configSoln(2).JointPosition configSoln(3).JointPosition]

disp("INVERSE KINEMATICS COMPUTED MANUALLY")
disp("WE HAVE TWO POSSIBLE ORIENTATION SOLUTIONS, ONE WILL BE DROPPED")
% Manual Computations
ik = data.ik
ik_num = getIKnumerical(pose)

disp("DIFFERENTIAL KINEMATICS SECTION");
disp("TOOLBOX GEOMETRIC JACOBIAN COMPUTATIONS");
J_toolbox = geometricJacobian(robot, config, 'ee')
disp("MANUAL GEOMETRIC JACOBIAN COMPUTATIONS");
J_num = getJGnumerical(q)

syms l0 l1 l2 l3 q1 q2 q3
pe = [
    l3*cos(q1+q3)+l1*cos(q1); 
    l3*sin(q1+q3)+l1*sin(q1);
    l0+l2+q2; 
    q1+q3
]
disp("TOOLBOX ANALYTICAL JACOBIAN COMPUTATIONS");
Ja_tool = jacobian(pe, [q1;q2;q3]);
Ja_tool = simplify(Ja_tool)
disp("MANUAL ANALYTICAL JACOBIAN COMPUTATIONS");
Ja = data.Ja
Ja_num = getJAnumerical(q)

if isequal(Ja_tool, Ja)
    disp("Equal Analytical Jacobians")
end

data.Ta = getTa(data.q);
%% Assignment 2
% Position of centre of mass wrt the frame attached to the link
data.com1 = [0.2, 0, 0]; 
data.com2 = [0, 0, 0.15];
data.com3 = [0.12, 0, 0];

% Inertia computations
[data.Ic1, data.Ic2, data.Ic3, data.I1, data.I2, data.I3, data.B, data.pl] = getInertia(data);

data.g = [0;0;-9.81]; % Gravity term
data.T = getKinetic(data); % Kinetic Energy
data.U = getPotential(data); % Potential Energy

%% Assignment 3: Compute the equations of motion, the Dynamic model
data.C = getCentrifugalCoriolis(data); % Coriolis Term
data.G = getGravity(data); % Gravity

% B(q)ddq + C(q, dq)dq + g(q) = tau
data.tau_L = getEquationsMotion(data); % Lagrangian Dynamic Model 

%% Assignment 4: Compute the RNE (Recursive Newton Euler) formulation
w0 = [0;0;0]; 
dw0 = [0;0;0]; 
data.he = [0;0;0;0;0;0];

data.tau_RNE = RNE(w0, dw0, data.he, data);
%% Evaluation to compare RNE with Lagrangian
l0 = 0.15;
l1 = 0.4;
l2 = 0.3;
l3 = 0.24;

q1 = q(1);
q2 = q(2);
q3 = q(3);

dq1 = dq(1);
dq2= dq(2);
dq3 = dq(3);

ddq1 = ddq(1);
ddq2= ddq(2);
ddq3 = ddq(3);

% dq1 = 0;
% dq2= 0;
% dq3 = 0;

% ddq1 = 0;
% ddq2= 0;
% ddq3 = 0;

tau_lagrangian = double(subs(data.tau_L))
tau_newton = double(subs(data.tau_RNE))

%% Assignment 5: Compute the dynamical model in the operational space
data.h = [1;1;1;1;1;1];

data.B = double(subs(data.B));
data.C = double(subs(data.C));
data.G = double(subs(data.G));
data.J = double(subs(data.J));
data.Jadot = double(subs(getJAdifferentiation()));
[data] = operational_space_dynamics(data);

%% Assignment 6: Design the Joint Space PD control law with gravity compensation

KD = [10;10;5];
KP = [500;500;500];

q0 = [0;0;0];
dq0 = q0;
ddq0 = q0;

qf = [-1.8249;-0.1343;0.8157];
dqf = dq0;
ddqf = ddq0;

% I need qd, dqd, ddqd
dof = 3;
ti = 0;
Ts = 0.01;
tf = 10;
t = ti : Ts : tf;

q1_i = q0(1);
q1_f = qf(1);

dq1_i = dq0(1);
dq1_f = dqf(1);
[q1_d, dq1_d, ddq1_d] = cubic_polynomial_trajectory(q1_i, q1_f, dq1_i, dq1_f, ti, tf);



q2_i = q0(2);
q2_f = qf(2);

dq2_i = dq0(2);
dq2_f = dqf(2);
[q2_d, dq2_d, ddq2_d] = cubic_polynomial_trajectory(q2_i, q2_f, dq2_i, dq2_f, ti, tf);

q3_i = q0(3);
q3_f = qf(3);

dq3_i = dq0(3);
dq3_f = dqf(3);
[q3_d, dq3_d, ddq3_d] = cubic_polynomial_trajectory(q3_i, q3_f, dq3_i, dq3_f, ti, tf);

q1_d = eval(q1_d);
dq1_d = eval(dq1_d); 
ddq1_d = eval(ddq1_d);

q2_d = eval(q2_d);
dq2_d = eval(dq2_d); 
ddq2_d = eval(ddq2_d);

q3_d = eval(q3_d);
dq3_d = eval(dq3_d); 
ddq3_d = eval(ddq3_d);

qd = [q1_d; q2_d; q3_d];
dqd = [dq1_d; dq2_d; dq3_d];
ddqd = [ddq1_d; ddq2_d; ddq3_d];


q_d.time=t;
q_d.signals.values=qd.';
q_d.signals.dimensions= dof;

dq_d.time=t;
dq_d.signals.values=dqd.';
dq_d.signals.dimensions=dof;


ddq_d.time=t;
ddq_d.signals.values=ddqd.';
ddq_d.signals.dimensions=dof;


config(1).JointPosition = qf(1);
config(2).JointPosition = qf(2);
config(3).JointPosition = qf(3);
show(robot,config);

%% Assignment 7: Design the Joint Space Inverse Dynamics Control Law
% I need qd, dqd, ddqd
q0 = [0;0;0];
qf = [1.5780; -0.2235; 0.0374] % returned has random conf previously
% KP = [20; 20; 20];
% KD = [2*(20^2);2*(20^2);2*(20^2)];

KD = [20;50;20];
KP = [80;300;100];

% If we put iswrong to 1 we use wrong B, C, g matrices
iswrong = 0;

dof = 3;
ti = 0;
Ts = 0.01;
tf = 10;
t = ti : Ts : tf;

q1_i = q0(1);
q1_f = qf(1);

dq1_i = dq0(1);
dq1_f = dq(1);
[q1_d, dq1_d, ddq1_d] = cubic_polynomial_trajectory(q1_i, q1_f, dq1_i, dq1_f, ti, tf);



q2_i = q0(2);
q2_f = qf(2);

dq2_i = dq0(2);
dq2_f = dq(2);
[q2_d, dq2_d, ddq2_d] = cubic_polynomial_trajectory(q2_i, q2_f, dq2_i, dq2_f, ti, tf);



q3_i = q0(3);
q3_f = qf(3);

dq3_i = dq0(3);
dq3_f = dq(3);
[q3_d, dq3_d, ddq3_d] = cubic_polynomial_trajectory(q3_i, q3_f, dq3_i, dq3_f, ti, tf);

q1_d = eval(q1_d);
dq1_d = eval(dq1_d); 
ddq1_d = eval(ddq1_d);

q2_d = eval(q2_d);
dq2_d = eval(dq2_d); 
ddq2_d = eval(ddq2_d);

q3_d = eval(q3_d);
dq3_d = eval(dq3_d); 
ddq3_d = eval(ddq3_d);

qd = [q1_d; q2_d; q3_d];
dqd = [dq1_d; dq2_d; dq3_d];
ddqd = [ddq1_d; ddq2_d; ddq3_d];


q_d.time=t;
q_d.signals.values=qd.';
q_d.signals.dimensions= dof;

dq_d.time=t;
dq_d.signals.values=dqd.';
dq_d.signals.dimensions=dof;


ddq_d.time=t;
ddq_d.signals.values=ddqd.';
ddq_d.signals.dimensions=dof;


%%
% Check that in the nominal case the dynamic behaviour is equivalent to the one of a
% set of stabilized double integrators
KD = [20;20;20];
KP = [100;100;100];
iswrong = 0;

%%
% Check the behavior of the control law when the ^B; ^C; ^g used within the controller are
% different than the “true ones” B;C; g (e.g. slightly modify the masses, the frictions, ...)
KD = [20;20;20];
KP = [100;100;100];
iswrong = 1;

%% What happens to the torque values when the settling time of the equivalent second
% order systems is chosen very small?
KD = [50;50;50];
KP = [1000;1000;1000]; % to have a high response
iswrong = 0;

%% assignment 8 : Implement in Simulink the Adaptive Control law for the 1 Dof link under gravity
% Amplitude for the inputs waves
A = 1;
% 1 DOF parameters
I = 0.4;
F = 0.2;
mgd = 0.6;
I_est = I-0.01;
F_est = F-0.01;
mgd_est = mgd-0.01;
lambda = 300;
K_theta = inv(diag([10000 100 100]));
KD = 80;

q0 = 0;
dq0 = 0;

%% assignment 9 : Design the Operational Space PD control law with gravity compensation

KD = [120;120;30;10;10;10];
KP = [1000;500;70;50;50;50];

q0 = [0;0;0]
dq0 = [0;0;0]
qf = [1.5780; -0.2235; 0.7] % returned has random conf previously

x0 = getDirectKinematics(q0)
xd = getDirectKinematics(qf)

config(1).JointPosition = qf(1);
config(2).JointPosition = qf(2);
config(3).JointPosition = qf(3);
show(robot, config);

%% assignment 10 : Design the Operational Space Inverse Dynamics Control Law

KD = [200;200;200;10;10;10];
KP = [7000;5000;3000;50;50;50];

q0 = [0;0;0];
dq0 = [0;0;0];
qf = [1.5780; -0.2235; 0.0374]; % returned has random conf previously

x0 = getDirectKinematics(q0);
xd = getDirectKinematics(qf);

config(1).JointPosition = qf(1);
config(2).JointPosition = qf(2);
config(3).JointPosition = qf(3);
show(robot, config);

[xd, ~, ~, ~] = rectilinear_path(x0(1:3), xd(1:3));


ti = 0;
Ts = 0.0106157112526539; % We must have the same values for time signals and trajectory
tf = 10;
t = ti : Ts : tf;

t = t(1:end-1);

x_d.time=t;
x_d.signals.values=[xd; zeros(3, length(xd))].'
x_d.signals.dimensions= 6;


%% assignment 11 :  Compliance control in the 2 extreme cases when the end effector is interacting with the environment
KD = [10;50;5;10;10;10];
KP = [1000;200;40;50;50;50];
K = diag([1 1 1 1 1 1]);

%%
KD = [10;50;5;10;10;10];
KP = [1000;200;40;50;50;50];
K = diag([1 1 40 1 1 1]);
%%
KD = [10;50;5;10;10;10];
KP = [1000;200;40;50;50;50];
K = diag([1 1 500 1 1 1]);
%%


q0 = [0;0;0];
dq0 = [0;0;0];

qf = [0;-0.3;0];

config(1).JointPosition = qf(1);
config(2).JointPosition = qf(2);
config(3).JointPosition = qf(3);
show(robot, config);

x0 = getDirectKinematics(q0);
xd = getDirectKinematics(qf);

% In this homework I just move the second joint to its limit position
% then to simulate the environment I use the same xd coordinates but
% translating a plane above with a factor of 0.1
xr = xd;
xr(3) = 0.25;

%% assignment 12 : implement the impedance control in simulink

KD = [200;40;10;10;10;10];
KP = [1000;200;50;50;50;50];
Md = diag([0.3;0.2;0.1;1;1;1]);
K = diag([1 1 0.1 1 1 1]);

q0 = [0;0;0];
dq0 = [0;0;0];

% with this configuration I bring the 2nd joint to its limit position
qf = [1.57;-0.3;0.8];

x0 = getDirectKinematics(q0)
xd = getDirectKinematics(qf)

% To simulate the environment I use the same xd coordinates but
% translating a plane above with a factor of 0.1

xr = xd;
xr(3) = 0.25;


config(1).JointPosition = qf(1);
config(2).JointPosition = qf(2);
config(3).JointPosition = qf(3);

show(robot, config);
%% assignment 13 : implement the force control with inner position loop

KD = [10;10;20;10;10;10];
KP = [50;50;20;50;50;50];
Md = diag([1;1;0.1;1;1;1]);
% K for the environment
K = diag([1 1 10 1 1 1]);
% CF definition
KF = 5;
% with just KF we do not reach the desired force
% KI = 0;
KI = 4;


q0 = [0;0;0];
dq0 = [0;0;0];

% with this configuration I bring the 2nd joint to its limit position
qf = [0;-0.3;0];

config(1).JointPosition = qf(1);
config(2).JointPosition = qf(2);
config(3).JointPosition = qf(3);

show(robot, config);

x0 = getDirectKinematics(q0);
xd = getDirectKinematics(qf);

% To simulate the environment I use the same xd coordinates but
% translating a plane above with a factor of 0.1

xr = xd;
xr(3) = 0.25;

fd = [0;0;-0.7;0;0;0];

%% assignment 14 : implement the parallel force/position loop

KD = [10;10;20;10;10;10];
KP = [50;50;20;50;50;50];
Md = diag([0.01;0.1;0.1;1;1;1]);
KI = 4;
KF = 5;

% Minimum K = 1 then you can increase as you want
K = diag([0 0 1 0 0 0]);

q0 = [0;0;0];
dq0 = [0;0;0];

% with this configuration I bring the 2nd joint to its limit position
qf = [1.57;-0.25;0];

config(1).JointPosition = qf(1);
config(2).JointPosition = qf(2);
config(3).JointPosition = qf(3);

config(1).JointPosition = q0(1);
config(2).JointPosition = q0(2);
config(3).JointPosition = q0(3);
show(robot, config);

x0 = getDirectKinematics(q0);
xd = getDirectKinematics(qf);

% To simulate the environment I use the same xd coordinates but
% translating a plane above with a factor of 0.1

xr = xd;
xr(3) = 0.25;

fd = [0;0;-0.05; 0;0;0];
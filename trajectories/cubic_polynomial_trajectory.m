function [q, dq, ddq] = cubic_polynomial_trajectory(qi, qf, dqi, dqf, ti, tf)
    syms q(t)
    q(t) = - ((2*qf - 2*qi - dqf*tf - dqi*tf + dqf*ti + dqi*ti)*t^3)/((tf - ti)*(tf^2 - 2*tf*ti + ti^2)) + ((3*qf*tf + 3*qf*ti - 3*qi*tf - 3*qi*ti - dqf*tf^2 - 2*dqi*tf^2 + 2*dqf*ti^2 + dqi*ti^2 - dqf*tf*ti + dqi*tf*ti)*t^2)/((tf - ti)*(tf^2 - 2*tf*ti + ti^2)) + ((dqi*tf^3 - dqf*ti^3 - dqf*tf*ti^2 + 2*dqf*tf^2*ti - 2*dqi*tf*ti^2 + dqi*tf^2*ti - 6*qf*tf*ti + 6*qi*tf*ti)*t)/((tf - ti)*(tf^2 - 2*tf*ti + ti^2)) - (qf*ti^3 - qi*tf^3 - dqf*tf*ti^3 + dqi*tf^3*ti - 3*qf*tf*ti^2 + 3*qi*tf^2*ti + dqf*tf^2*ti^2 - dqi*tf^2*ti^2)/((tf - ti)*(tf^2 - 2*tf*ti + ti^2));
    dq(t) = diff(q);
    ddq(t) = diff(q, 2);
end


% Function to compute the geometric jacobian given the joint positions
function [JG1, JG2, JG3, pl] = getJGli(data)
% GETJGLI computes the geometrix jacobians for the center of masses of my
% RPR robot
    syms l0 l1 l2 l3
    % Link length
    l0 = 0.15;
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24;

    % pli are the position of the COM wrt the base frame Eb
    
    p0 = data.Ab0;
    p0 = p0(1:3, 4);

    S = [0 -1 0; 1 0 0; 0 0 0];
    
    p1 = data.Ab0 * data.A01;
    p1 = p1(1:3, 4);
    % Position of 1st center of mass
    % -data.com1 to express it in the E1
    pl1 = data.Ab0 * data.A01 * [-data.com1.';1];
    pl1 = pl1(1:3);
    
    JG1 = sym(zeros([6,3]));
    JG1(4:6, 1) = S * (pl1 - p0); % related to linear velocity
    JG1(1:3, 1) = [0;0;1]; % related to angular velocity
    
    p2 = data.Ab0 * data.A01 * data.A12;
    p2 = p2(1:3, 4);
    % Position of 2nd center of mass
    % -data.com2 to express it in the E2
    pl2 = data.Ab0 * data.A01 * data.A12 * [-data.com2.';1];
    pl2 = pl2(1:3);
    
    JG2 = sym(zeros([6,3]));
    
    JG2(4:6, 1) = S * (pl2 - p0);
    JG2(1:3, 1) = [0;0;1];
    
    JG2(4:6, 2) = [0;0;1];
    JG2(1:3, 2) = [0;0;0];
    
    % Position of 3rd center of mass
    
    % -data.com3 to express it in the E3
    pl3 = data.Ab0 * data.A01 * data.A12 * data.A23 * [-data.com3.';1];
    pl3 = pl3(1:3);
    
    JG3 = sym(zeros([6,3]));
    JG3(4:6, 1) = S * (pl3 - p0);
    JG3(1:3, 1) = [0;0;1];
    
    JG3(4:6, 2) = [0;0;1];
    JG3(1:3, 2) = [0;0;0];
    
    JG3(4:6, 3) = S * (pl3 - p2);
    JG3(1:3, 3) = [0;0;1];
    
    
    JG1 = simplify(JG1);
    JG2 = simplify(JG2);
    JG3 = simplify(JG3);
    
    JG1 = subs(JG1);
    JG2 = subs(JG2);
    JG3 = subs(JG3);
    
    pl1 = subs(pl1);
    pl2 = subs(pl2);
    pl3 = subs(pl3);
    
    pl = [pl1 pl2 pl3];
    
end
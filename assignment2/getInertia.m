function [Ic1, Ic2, Ic3, I01, I12, I23, B, pl] = getInertia(data)
%INERTIA compute the inertia wrt the center of mass
    syms q1 q2 q3
    
%     l0 = 0.15;
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24;
    
    m1 = data.m1; 
    m2 = data.m2; 
    m3 = data.m3;
    
    r1 = 0.02;
    r3 = 0.02;
    
    Ic1 = [0.5*m1*r1^2 0 0; 
          0 (1/12)*m1*(3*r1^2 + l1^2) 0;
          0 0 (1/12)*m1*(3*r1^2 + l1^2)];

    % Link 3 dimensions: 0.03 0.03 0.3 (parallelepiped)
    % l2 = 0.3
    Ic2 = [(1/12)*m2*(l2^2 + 0.03^2) 0 0;
          0 (1/12)*m2*(l2^2 + 0.03^2) 0;
          0 0 (1/12)*m2*(0.03^2 + 0.03^2)];

    Ic3 = [0.5*m3*r3^2 0 0; 
          0 (1/12)*m3*(3*r3^2 + l3^2) 0;
          0 0 (1/12)*m3*(3*r3^2 + l3^2)];
      
    com1 = data.com1;
    com2 = data.com2;
    com3 = data.com3;

    S1 = [
        0 -com1(3) com1(2);
        com1(3) 0 -com1(1);
        -com1(2) com1(1) 0
    ];

    S2 = [
        0 -com2(3) com2(2);
        com2(3) 0 -com2(1);
        -com2(2) com2(1) 0
    ];

    S3 = [
        0 -com3(3) com3(2);
        com3(3) 0 -com3(1);
        -com3(2) com3(1) 0
    ];
    
    % I translate the frame backwards and I have to express the
    % translations wrt the frame attached to the centre of mass
    % So change the sign in the S matrices!
    S1 = -S1;
    S2 = -S2;
    S3 = -S3;

    I01 = Ic1 + m1 * (S1.'*S1);
    I12 = Ic2 + m2 * (S2.'*S2);
    I23 = Ic3 + m3 * (S3.'*S3);

    % OTHER WAY OF APPLYING THE STEINER THEOREM

%     rcom1 = [-com1(1); -com1(2); -com1(3)]; % bring the frame to E0
%     rcom2 = [-com2(1); -com2(2); -com2(3)]; % bring the frame to E1 
%     rcom3 = [-com3(1); -com3(2); -com3(3)]; % bring the frame to E2

%     rcom1 = -com1.'; % bring the frame to E1
%     rcom2 = -com2.'; % bring the frame to E2
%     rcom3 = -com3.'; % bring the frame to E3
% 
%     I01 = Ic1 + m1 * (rcom1.'*rcom1*eye(3) - rcom1*rcom1.');
%     I12 = Ic2 + m2 * (rcom2.'*rcom2*eye(3) - rcom2*rcom2.');
%     I23 = Ic3 + m3 * (rcom3.'*rcom3*eye(3) - rcom3*rcom3.');
    
%     J = data.J;
    Ab0 = data.Ab0;
    A01 = data.A01;
    A12 = data.A12;
    A23 = data.A23;
    
    [J1, J2, J3, pl] = getJGli(data);
    
    % For link 1
    Jp1 = J1(4:6, :);
    Jo1 = J1(1:3, :);
    % For link 2
    Jp2 = J2(4:6, :);
    Jo2 = J2(1:3, :);
    % For link 3
    Jp3 = J3(4:6, :);
    Jo3 = J3(1:3, :);
    
    Rb0 = Ab0(1:3, 1:3);
    R01 = A01(1:3, 1:3);
    R12 = A12(1:3, 1:3);
    R23 = A23(1:3, 1:3);

%     Rb0 = Rb0;
    Rb1 = Rb0*R01;
    Rb2 = Rb1*R12;

    b1 = m1*(Jp1.'*Jp1) + Jo1.'*(Rb0*I01*Rb0.')*Jo1;
    b2 = m2*(Jp2.'*Jp2) + Jo2.'*(Rb1*I12*Rb1.')*Jo2;
    b3 = m3*(Jp3.'*Jp3) + Jo3.'*(Rb2*I23*Rb2.')*Jo3;

    B = b1 + b2 + b3;
    B = subs(B);
    B = simplify(B);
    
    assumeAlso(q1, 'real');
    assumeAlso(q2, 'real');
    assumeAlso(q3, 'real');
    assumeAlso(-pi<=q1 & q1<=pi);
    assumeAlso(-pi<=q3 & q3<=pi);
    assumeAlso(-0.3<=q2 & q2<=0);
    assumptions;

    if (isAlways(B == B.'))
        disp("B is symmetric");
    else
        disp("B isn't symmetric");
    end

    if isAlways(det(B) ~= 0)
        disp("B is positive definite");
    else
        disp("B isn't positive definite");
    end

    if (isreal(B(3,3)))
        disp("The element in the last column and row is a constant real number");
    else
        disp("Third element in third row not constant");
    end
end


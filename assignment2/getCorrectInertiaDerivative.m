function [B_diff] = getCorrectInertiaDerivative()
syms q1 q2 q3 dq1 dq2 dq3
B_diff = [[-(90432*sin(q3)*dq3)/390625, 0, -(45216*sin(q3)*dq3)/390625];
[                      0, 0,                       0];
[-(45216*sin(q3)*dq3)/390625, 0,                       0]];
end


function [T] = getKinetic(data)
    % compute the kinetic energy
    syms dq1 dq2 dq3
    dq = [dq1; dq2; dq3]; % check because qdot has to be function of time
    T = 1/2*dq.'*data.B*dq;
    T = simplify(T);
end


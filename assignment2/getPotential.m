function [U] = getPotential(data)
syms l0 l1 l2 l3
    % Link length
    l0 = 0.15;
    l1 = 0.4; 
    l2 = 0.3; 
    l3 = 0.24;
    
    g = data.g;
    
    Ab0 = data.Ab0; 
    A01 = data.A01; 
    A12 = data.A12; 
    A23 = data.A23;
    
    pl1 = Ab0*A01 * [data.com1.';1];
    pl2 = Ab0*A01*A12 * [data.com2.';1];
    pl3 = Ab0*A01*A12*A23 * [data.com3.';1];

    U1 = data.m1*g.'*pl1(1:3);
    U2 = data.m2*g.'*pl2(1:3);
    U3 = data.m3*g.'*pl3(1:3);

    U = -(U1 + U2 + U3);
    U = simplify(U);
    
    U = subs(U);
    
end


function [T_num] = getKineticnumerical(q, dq)
    q1 = q(1); q2 = q(2); q3 = q(3);
    dq1 = dq(1); dq2 = dq(2); dq3 = dq(3);
    T_num = (3097*dq1*dq3)/8000 + (216863*dq1^2)/48000 + (15*dq2^2)/2 + (3097*dq3^2)/16000 + (84*dq1^2*cos(q3))/25 + (12*dq1*dq3*cos(q3))/25;
end


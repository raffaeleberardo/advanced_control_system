function [B_num] = getInertianumerical(q)

    q1 = q(1);
    q2 = q(2);
    q3 = q(3);

    
    B_num = [(168*cos(q3))/25 + 216863/24000,  0, (12*cos(q3))/25 + 3097/8000;
                             0, 15,                           0;
                (12*cos(q3))/25 + 3097/8000,  0,                   3097/8000];        
end


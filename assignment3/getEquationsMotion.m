function [tau] = getEquationsMotion(data)
    syms q1 q2 q3 dq1 dq2 dq3 ddq1 ddq2 ddq3
    ddq = [ddq1; ddq2; ddq3];
    dq = [dq1; dq2; dq3];
    q = [q1;q2;q3];
    tau = data.B * ddq + data.C * dq + data.G;
    tau = simplify(tau);
end


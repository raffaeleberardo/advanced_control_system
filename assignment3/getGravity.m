function [G] = getGravity(data)
    syms q1 q2 q3
    q = [q1 q2 q3];

    l0 = data.links(1); 
    l1 = data.links(2); 
    l2 = data.links(3); 
    l3 = data.links(4);
    
    Ab0 = data.Ab0;
    A01 = data.A01;
    A12 = data.A12;
    A23 = data.A23;
    
%     p1 = Ab0*A01 * [data.com1.';1];
%     p2 = Ab0*A01*A12 * [data.com2.';1];
%     p3 = Ab0*A01*A12*A23 * [data.com3.';1];

%     p = data.pl
%     p(4, :) = []
    
    [J1, J2, J3, ~] = getJGli(data);
    
    % For link 1
    Jp1 = J1(4:6, :);
    % For link 2
    Jp2 = J2(4:6, :);
    % For link 3
    Jp3 = J3(4:6, :);
    
    Jp = cat(3, Jp1, Jp2, Jp3);
    
    m = [data.m1 data.m2 data.m3];
    g = data.g;
    G = [0;0;0];
    for i=1:length(q)
        gi = 0;
        for j = 1:length(q)
            gi = gi + (m(j) * g.' * Jp(:, i, j));
        end
%         gi = subs(gi);
        G(i) = -gi;
    end
end


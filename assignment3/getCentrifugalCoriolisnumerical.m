function [C_num] = getCentrifugalCoriolisnumerical(q, dq)
q1 = q(1); q2 = q(2); q3 = q(3);
dq1 = dq(1); dq2 = dq(2); dq3 = dq(3);
C_num = [-(84*dq3*sin(q3))/25, 0, -(12*sin(q3)*(7*dq1 + dq3))/25;
                   0, 0,                              0;
(84*dq1*sin(q3))/25, 0,                              0];
end


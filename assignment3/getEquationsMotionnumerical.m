function [tau] = getEquationsMotionnumerical(q, dq, ddq)
q1 = q(1); 
q2 = q(2); 
q3 = q(3);

dq1 = dq(1); 
dq2 = dq(2); 
dq3 = dq(3);

ddq1 = ddq(1); 
ddq2 = ddq(2);
ddq3 = ddq(3);

% tau = [ddq3*((12*cos(q3))/25 + 3097/8000) + ddq1*((168*cos(q3))/25 + 216863/24000) - (84*dq1*dq3*sin(q3))/25 - (12*dq3*sin(q3)*(7*dq1 + dq3))/25;
%                                                                                                                         15*ddq2 + 2943/20;
%                                                             (84*sin(q3)*dq1^2)/25 + (3097*ddq3)/8000 + ddq1*((12*cos(q3))/25 + 3097/8000)];
tau = [ddq3*((6*cos(q3))/25 + 1369/8000) + ddq1*((12*cos(q3))/25 + 106591/24000) - (6*dq1*dq3*sin(q3))/25 - (6*dq3*sin(q3)*(dq1 + dq3))/25;
                                                                                                                  15*ddq2 + 2943/20;
                                                        (6*sin(q3)*dq1^2)/25 + (1369*ddq3)/8000 + ddq1*((6*cos(q3))/25 + 1369/8000)]
end


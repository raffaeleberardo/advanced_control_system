function [C] = getCentrifugalCoriolis(data)
    % Compute the coriolis and centrifugal matrix using the Chrystoffel Symbols
    % of the 1st type
    syms q1 q2 q3 dq1 dq2 dq3
   
    q = [q1;q2;q3];
    dq = [dq1;dq2;dq3];
    
    B = data.B;
    C = sym(zeros(3,3));
    
    
    
    for i = 1:length(q)
        for j = 1:length(q)
            cij = 0;
            for k = 1:length(q)
                bij = B(i, j);
                bik = B(i, k);
                bjk = B(j, k);
                cijk = 0.5 * (diff(bij, q(k)) + diff(bik, q(j)) - diff(bjk, q(i)));
                cij = cij + (cijk * dq(k));
            end
            C(i, j) = cij;
        end
    end
    C = simplify(C);
    [B_derivate] = getCorrectInertiaDerivative();
    w = rand([3 1]);
    isSkewSymmetric = w.' * simplify(B_derivate - (2 * C)) * w;
    if (isSkewSymmetric == 0)
        disp('Bdot - 2C is skew symmetric');
    end
    
end

